## Introduction to Git

### Description: 
A specialized tutorial for the HPC team at NYU on Git. 

### Topics: 
Git, command line, version control.

### Thank You
A big big thank you to [Andy Mueller](https://github.com/amueller), who originally taught this class at NYU amd taught me all I know about Git and GitHub! These slides are heavily influenced by his previous class materials, [found here](https://github.com/amueller/git_workshop). I also got a few images and some ideas for content from the [Atlassian tutorial](https://www.atlassian.com/git/tutorials/what-is-version-control)! It's so great and the images are lovely.
